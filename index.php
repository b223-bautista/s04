<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>S04: Access Modifiers</title>
</head>
<body>
	<h2>Building</h1>
	<p>The name of the building is <?php echo $building->getName(); ?></p>
	<p>The <?php echo $building->getName(); ?> has <?php echo $building->getFloor(); ?> floors</p>
	<p>The <?php echo $building->getName(); ?> is located at <?php echo $building->getAddress(); ?></p>
	<p>The name of the building has been changed to <?php echo $building->setName('Caswynn Complex') ?></p>

	<h2>Condominium</h1>
	<p>The name of the condominium is <?php echo $condominium->getName(); ?></p>
	<p>The <?php echo $condominium->getName(); ?> has <?php echo $condominium->getFloor(); ?> floors</p>
	<p>The <?php echo $condominium->getName(); ?> is located at <?php echo $condominium->getAddress('Buedia Avenue, Makati City, Philippines.'); ?></p>
	
		<p>The name of the building has been changed to <?php echo $condominium->setName('Enzo Tower'); ?></p>

</body>
</html>